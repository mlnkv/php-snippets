<?php

# how mysqli_prepare should work
# https://github.com/noodlehaus/mysqli_etc
# Expansion of mysqli_prepare
function mysqli_interpolate($db, $sql) {

  $args = array_slice(func_get_args(), 2);
  $argn = count($args);
  $stmt = mysqli_prepare($db, (string) $sql);

  if ($stmt === false) {
    throw new mysqli_sql_exception(mysqli_error($db), mysqli_errno($db));
  }

  if ($argn) {

    $syms = implode('', array_pad([], $argn, 's'));
    $refs = [];

    foreach ($args as $key => $val) {
      $refs[$key] = &$args[$key];
    }

    if (false === call_user_func_array('mysqli_stmt_bind_param', array_merge([$stmt, $syms], $refs))) {
      throw new mysqli_sql_exception(
        mysqli_stmt_error($stmt),
        mysqli_stmt_errno($stmt)
      );
    }
  }

  return $stmt;
}

# Executes an insert and returns the insert id if any.
function mysqli_insert($db, $sql) {

  $stmt = call_user_func_array('mysqli_interpolate', func_get_args());

  if (!mysqli_stmt_execute($stmt)) {
    throw new mysqli_sql_exception(
      mysqli_stmt_error($stmt),
      mysqli_stmt_errno($stmt)
    );
  }

  $id = mysqli_insert_id($db);
  mysqli_stmt_close($stmt);

  return (string) $id;
}

# Executes a select and returns all resulting rows
function mysqli_select($db, $sql) {

  $stmt = call_user_func_array('mysqli_interpolate', func_get_args());

  if (!mysqli_stmt_execute($stmt) || (false === ($result = mysqli_stmt_get_result($stmt)))) {
    throw new mysqli_sql_exception(
      mysqli_stmt_error($stmt),
      mysqli_stmt_errno($stmt)
    );
  }

  $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);

  mysqli_free_result($result);
  mysqli_stmt_close($stmt);

  return (array) $rows;
}

# Executes an update/delete query and returns affected row count.
function mysqli_update($db, $sql) {

  $stmt = call_user_func_array('mysqli_interpolate', func_get_args());

  if (!mysqli_stmt_execute($stmt)) {
    throw new mysqli_sql_exception(
      mysqli_stmt_error($stmt),
      mysqli_stmt_errno($stmt)
    );
  }

  $affected = mysqli_stmt_affected_rows($stmt);
  mysqli_stmt_close($stmt);

  return (int) $affected;
}

function db_query($db, $sql) {

  $stmt = call_user_func_array('mysqli_interpolate', func_get_args());

  if (!mysqli_stmt_execute($stmt)) {
    throw new mysqli_sql_exception(
      mysqli_stmt_error($stmt),
      mysqli_stmt_errno($stmt)
    );
  }

  if (stripos(trim($sql), 'INSERT') === 0) {
    $id = mysqli_insert_id($db);
    mysqli_stmt_close($stmt);

    return (string) $id;
  }

  if (stripos(trim($sql), 'SELECT') === 0) {
    if (false === ($result = mysqli_stmt_get_result($stmt))) {
      throw new mysqli_sql_exception(
        mysqli_stmt_error($stmt),
        mysqli_stmt_errno($stmt)
      );
    }

    $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);

    mysqli_free_result($result);
    mysqli_stmt_close($stmt);

    return (array) $rows;
  }

  if (stripos(trim($sql), 'UPDATE') === 0 || stripos(trim($sql), 'DELETE') === 0) {
    $affected = mysqli_stmt_affected_rows($stmt);
    mysqli_stmt_close($stmt);

    return (int) $affected;
  }
}

?>