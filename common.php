<?php

function dd() {
  echo '<pre>';
  call_user_func_array('var_dump', func_get_args());
  exit;
}

function debug($text) {
  $path = "log.txt";
  $base = dirname($path);

  if ($base !== '.' && !file_exists($base)) {
    throw new Exception("Directory {$base} doesn't exist.");
  }
  if (touch($path) === false) {
    throw new Exception("Unable to create log file {$path}.");
  }

  $t = strftime('%Y-%m-%d %H:%M:%S');
  $s = sprintf('[%s]: '.$text.PHP_EOL, $t, array_slice(func_get_args(), 1));
  file_put_contents($path, $s, LOCK_EX|FILE_APPEND);
}

?>